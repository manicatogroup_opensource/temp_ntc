//***************************************************************************
//* TempNTC.C
//*--------------------------------------------------------------------------
//* Rutinas de conversi�n de temperaturas a partir de valores del conversor
//*--------------------------------------------------------------------------
//* (c) Beamspot. Marzo 2008. Versi�n 1.0
//***************************************************************************
/*
Licencia cervecera.
Cualquiera es libre de usar este programa cuando y como le plazca. Si alg�n dia
nos encontramos, y esto le ha sido de utilidad, siempre me puede invitar a una
cerveza u otra bebida que nos venga a gusto.;)

Declino cualquier responsabilidad sobre el uso de dicho programa. No doy ninguna
garant�a respecto del correcto funcionamiento y/o compilaci�n.

Este programa ha sido compilado con IAR 5.10 para AVR y con WinAVR/AVRStudio,
y probado sobre un ATmega64.
*/

#include "TempNTC.h"

//** Tabla de linealizaci�n de la NTC


const unsigned int TablaADC[PuntosTabla]=
 {3939, 3820, 3636, 3777, 3035, 2637, 2209, 1793, 1421, 1104, 842, 645, 492,  378,  293,  231,  178};
// -40,  -30,  -20,  -10,    0,   10,   20,   30,   40,   50,  60,  70,  80,   90,  100,  110,  120, �C
//   0     1     2     3     4     5     6     7     8     9   10   11   12    13    14    15    16  i

//***************************************************************************
//*  Funci�n TempNTC
//*  Devuelve el valor num�rico de la temperatura obtenido por interpolaci�n
//*  ADC es el valor devuelto por el conversor ADC
//*  To es el valor de la temperatura para el �ndice 0 de la tabla (-30�C)
//*  dT es la diferencia de temperatura entre dos entradas de la tabla (10�C)
//***************************************************************************
int TempNTC(unsigned int ADC, int To, int dT)
{
  int aux;
  unsigned int min, max;
  char i;
  
  //Buscamos el intervalo de la tabla en que se encuentra el valor de ADC
  for(i=0;(i<PuntosTabla)&&(ADC<(TablaADC[i])); i++);
  if ((i==0)||(i==PuntosTabla)) //Si no est�, devolvemos un error
    return -32767;
  max=TablaADC[i-1]; //Buscamos el valor m�s alto del intervalo
  min=TablaADC[i];   //y el m�s bajoa
  aux=(max-ADC)*dT;  //hacemos el primer paso de la interpolaci�n
  aux=aux/(max-min); //y el segundo paso
  aux+=(i-1)*dT+To;  //y a�adimos el offset del resultado
  return aux;
}
//************************ Fin **********************************************
