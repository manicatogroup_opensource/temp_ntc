//***************************************************************************
//* TempNTC.h
//*--------------------------------------------------------------------------
//* Declaraciones y macros para la conversi�n de temperaturas
//*--------------------------------------------------------------------------
//* (c) Beamspot. Marzo 2008. Versi�n 1.0
//***************************************************************************

//Mecanismo 'antiduplicidad'
#ifndef _TempNTC_H
#define _TempNTC_H

//Definiciones de constantes para la conversi�n
#define PuntosTabla   (18)
//Par�metros para conversi�n en �Celsius
#define ToCels      (-300)
#define dTCels       (100)
//Par�metros para conversi�n en Kelvins
#define ToKelv      (2430)
#define dTKelv       (100)
//Par�metros para conversi�n en �Fahrenheit
#define ToFahr      (-220)
#define dTFahr       (180)

//Prototipos de funciones a exportar del m�dulo
int TempNTC(unsigned int ADC, int To, int dT);

#endif
